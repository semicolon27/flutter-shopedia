import 'package:flutter/material.dart';
import 'package:shopedia/SearchPage.dart';

void main() => runApp(MaterialApp(
      title: "Shopedia",
      home: SearchPage(),
    ));
