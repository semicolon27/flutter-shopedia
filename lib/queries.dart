String searchQuery() {
  return r"""
  query SearchProductQuery ($params: String){
      searchProduct(params: $params) {
          source
          totalData: count
          totalDataText: count_text
          additionalParams: additional_params
          redirection {
              redirectionURL: redirect_url
              departmentID: department_id
              __typename
          }
          responseCode: response_code
          keywordProcess: keyword_process
          suggestion {
              suggestion
              suggestionCount
              currentKeyword
              instead
              insteadCount
              suggestionText: text
              suggestionTextQuery: query
              __typename
          }
          related {
              relatedKeyword: related_keyword
              otherRelated: other_related {
                  keyword
                  url
                  __typename
              }
              __typename
          }
          isQuerySafe
          ticker {
              text
              query
              __typename
          }
          catalogs {
              id
              name
              price
              priceMin: price_min
              priceMax: price_max
              countProduct: count_product
              description
              imageURL: image_url
              url
              category: department_id
              __typename
          }
          products {
              id
              name
              childs
              url
              imageURL: image_url
              imageURL300: image_url_300
              imageURL500: image_url_500
              imageURL700: image_url_700
              price
              priceRange: price_range
              category: department_id
              categoryID: category_id
              categoryName: category_name
              categoryBreadcrumb: category_breadcrumb
              discountPercentage: discount_percentage
              originalPrice: original_price
              shop {
                  id
                  name
                  url
                  isPowerBadge: is_power_badge
                  isOfficial: is_official
                  location
                  city
                  reputation
                  clover
                  __typename
              }
              wholesalePrice: whole_sale_price {
                  quantityMin: quantity_min
                  quantityMax: quantity_max
                  price
                  __typename
              }
              courierCount: courier_count
              condition
              labels {
                  title
                  color
                  __typename
              }
              labelGroups: label_groups {
                  position
                  type
                  title
                  __typename
              }
              badges {
                  title
                  imageURL: image_url
                  show
                  __typename
              }
              isFeatured: is_featured
              rating
              countReview: count_review
              stock
              GAKey: ga_key
              preorder: is_preorder
              wishlist
              shop {
                  id
                  name
                  url
                  goldmerchant: is_power_badge
                  location
                  city
                  reputation
                  clover
                  official: is_official
                  __typename
              }
              __typename
          }
          __typename
      }
  }""";
}

String detailQuery() {
  return r'''
  query PDPInfoQuery($shopDomain: String, $productKey: String) {
            getPDPInfo(productID: 0, shopDomain: $shopDomain, productKey: $productKey) {
                basic {
                    id
                    shopID
                    name
                    alias
                    price
                    priceCurrency
                    lastUpdatePrice
                    description
                    minOrder
                    maxOrder
                    status
                    weight
                    weightUnit
                    condition
                    url
                    sku
                    gtin
                    isKreasiLokal
                    isMustInsurance
                    isEligibleCOD
                    isLeasing
                    catalogID
                    needPrescription
                    __typename
                }
                category {
                    id
                    name
                    title
                    breadcrumbURL
                    isAdult
                    detail {
                        id
                        name
                        breadcrumbURL
                        __typename
                    }
                    __typename
                }
                pictures {
                    picID
                    fileName
                    filePath
                    description
                    isFromIG
                    width
                    height
                    urlOriginal
                    urlThumbnail
                    url300
                    status
                    __typename
                }
                preorder {
                    isActive
                    duration
                    timeUnit
                    __typename
                }
                wholesale {
                    minQty
                    price
                    __typename
                }
                videos {
                    source
                    url
                    __typename
                }
                campaign {
                    campaignID
                    campaignType
                    campaignTypeName
                    originalPrice
                    discountedPrice
                    isAppsOnly
                    isActive
                    percentageAmount
                    stock
                    originalStock
                    startDate
                    endDate
                    endDateUnix
                    appLinks
                    hideGimmick
                    __typename
                }
                stats {
                    countView
                    countReview
                    countTalk
                    rating
                    __typename
                }
                txStats {
                    txSuccess
                    txReject
                    itemSold
                    __typename
                }
                cashback {
                    percentage
                    __typename
                }
                variant {
                    parentID
                    isVariant
                    __typename
                }
                stock {
                    useStock
                    value
                    stockWording
                    __typename
                }
                menu {
                    name
                    __typename
                }
                __typename
            }
}
  ''';
}
