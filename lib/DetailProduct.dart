import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/scheduler.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:shopedia/config.dart';
import 'package:shopedia/queries.dart' as queries;

class DetailProductPage extends StatefulWidget {
  const DetailProductPage({Key key, this.imageTag, this.imageURL, this.url})
      : super(key: key);
  final String imageTag;
  final String imageURL;
  final String url;

  @override
  _DetailProductPageState createState() => _DetailProductPageState();
}

class _DetailProductPageState extends State<DetailProductPage> {
  @override
  Widget build(BuildContext context) {
    String shopDomain = widget.url.split("/")[3];
    String productKey = widget.url.split("/")[4].split("?")[0];
    final String query = queries.detailQuery();
    var pics = [];
    setState(() => pics.add(widget.imageURL));
    return GraphQLProvider(
      client: client,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Detail Produk"),
        ),
        body: Query(
            options: QueryOptions(
              document: query,
              variables: <String, dynamic>{
                "shopDomain": shopDomain,
                "productKey": productKey
              },
            ),
            builder: (
              QueryResult result, {
              FetchMore fetchMore,
              VoidCallback refetch,
            }) {
              if (result.loading) {
                return ListView(children: <Widget>[
                  Hero(
                    tag: widget.imageTag,
                    child: Image.network(
                      widget.imageURL,
                      loadingBuilder: (context, child, progress) {
                        return progress == null
                            ? child
                            : Center(
                                child: CircularProgressIndicator(),
                              );
                      },
                    ),
                  ),
                  Center(child: CircularProgressIndicator()),
                ]);
              }
              if (result.data == null) {
                return Text("No Data Found !");
              }
              dynamic data = result.data['getPDPInfo'];
              // int i;
              // for (i = 1; i < data['pictures']; i++) {
              //   pics.add(data['pictures'].pop);
              // }
              pics = data['pictures'].map((v) => v['urlOriginal']).toList();
              return ListView(
                children: <Widget>[
                  CarouselSlider(
                    // height: 400.0,
                    viewportFraction: 1.0,
                    enableInfiniteScroll: false,
                    autoPlay: false,
                    items: pics.map((i) {
                      // print(i);
                      return Hero(
                          tag: widget.imageTag,
                          child: Image.network(i,
                              loadingBuilder: (context, child, progress) {
                            return progress == null
                                ? child
                                : Center(
                                    child: CircularProgressIndicator(),
                                  );
                          }));
                    }).toList(),
                  ),
                  Container(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        children: <Widget>[
                          Text(
                            data['basic']['name'],
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: Text(
                              data['basic']['price'].toString(),
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                            alignment: Alignment.centerLeft,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(data['basic']['description']),
                        ],
                      )),
                ],
              );
            }),
      ),
    );
  }
}

// Text(products[0]['url'].split("/")[3]),
//               Text(products[0]['url'].split("/")[4].split("?")[0]),
