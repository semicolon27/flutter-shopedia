import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final HttpLink apiLink = HttpLink(uri: "https://gql.tokopedia.com/");
final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
  GraphQLClient(
      link: apiLink,
      cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject)),
);
