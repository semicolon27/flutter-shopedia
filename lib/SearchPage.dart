import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shopedia/DetailProduct.dart';
import 'package:shopedia/config.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shopedia/queries.dart' as queries;

class SearchPage extends StatelessWidget {
  const SearchPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(client: client, child: ProductListWidget());
  }
}

class ProductListWidget extends StatefulWidget {
  ProductListWidget({Key key}) : super(key: key);

  @override
  _ProductListWidgetState createState() => _ProductListWidgetState();
}

class _ProductListWidgetState extends State<ProductListWidget> {
  final String query = queries.searchQuery();
  final searchInputController = TextEditingController();
  String keyword = "laptop";
  bool isSearchFocused = false;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    searchInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // bool isScreenWide = MediaQuery.of(context).size.width >= kMinWidthOfLargeScreen;
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Shopedia"),
      // ),
      body: Query(
        options: QueryOptions(
          document: query,
          variables: <String, dynamic>{
            "params":
                "scheme=https&device=desktop&related=true&st=product&q=$keyword&ob=23&page=1&start=0&rows=10&user_id=12440294&unique_id=e10b210428ab4b2d9287db239ac6dba2&safe_search=false&source=search&nuq="
          },
        ),
        builder: (
          QueryResult result, {
          FetchMore fetchMore,
          VoidCallback refetch,
        }) {
          if (result.loading) {
            return Center(child: CircularProgressIndicator());
          }
          if (result.data == null) {
            return Text("No Data Found !");
          }
          dynamic products = result.data['searchProduct']['products'];
          return CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                leading: IconButton(
                  icon: Icon(Icons.list),
                  onPressed: () => print("Menu"),
                ),
                title: Text("Shopedia"),
                expandedHeight: 90,
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.person),
                      onPressed: () => print("Contact")),
                  IconButton(
                      icon: Icon(Icons.more_vert),
                      onPressed: () => print("More")),
                ],
                // Text("Shopedia"),
                bottom: PreferredSize(
                  preferredSize: const Size.fromHeight(30.0),
                  child: Theme(
                      data:
                          Theme.of(context).copyWith(accentColor: Colors.white),
                      child: Container(
                        height: 40,
                        margin: EdgeInsets.only(
                          top: 0,
                          bottom: 10,
                          left: 10,
                          right: 10,
                        ),
                        color: Colors.blue,
                        child: Column(
                          children: <Widget>[
                            Container(
                              // height: 30,
                              decoration: BoxDecoration(color: Colors.white),
                              child: TextField(
                                  decoration: InputDecoration(
                                    hintText: "Search",
                                    prefixIcon: Icon(Icons.search,
                                        // color: isSearchFocused ? Colors.grey : Colors.white,
                                        color: Colors.grey),
                                    border: InputBorder.none,
                                    fillColor: Colors.white,
                                  ),
                                  controller: searchInputController,
                                  textInputAction: TextInputAction.search,
                                  onSubmitted: (String value) {
                                    setState(() => keyword = value);
                                    searchInputController.clear();
                                  }),
                            ),
                            FlatButton(
                              child: Text("c"),
                              onPressed: () => setState(
                                  () => keyword = searchInputController.text),
                            )
                          ],
                        ),
                      )),
                ),
              ),
              SliverPadding(
                padding: EdgeInsets.all(10),
                sliver: SliverGrid(
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8,
                    childAspectRatio: 0.63,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailProductPage(
                                imageTag: products[index]['id'].toString(),
                                imageURL: products[index]['imageURL700'],
                                url: products[index]['url'],
                              ),
                            ),
                          );
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                color: Colors.white),
                            child: Column(
                              children: <Widget>[
                                AspectRatio(
                                  aspectRatio: 1 / 1,
                                  child: Hero(
                                    tag: products[index]['id'].toString(),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image.network(
                                          products[index]['imageURL700'],
                                          fit: BoxFit.fitWidth, loadingBuilder:
                                              (context, child, progress) {
                                        return progress == null
                                            ? child
                                            : Center(
                                                child:
                                                    CircularProgressIndicator(),
                                              );
                                      }),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 7, right: 7),
                                  child: Text(
                                    products[index]['name'],
                                    style: TextStyle(fontSize: 11.0),
                                    softWrap: false,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 5),
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    children: <Widget>[
                                      RatingBar(
                                        itemSize: 15.0,
                                        initialRating:
                                            products[index]['rating'] * 1.0,
                                        minRating: 0.5,
                                        direction: Axis.horizontal,
                                        allowHalfRating: true,
                                        itemCount: 5,
                                        itemPadding: EdgeInsets.symmetric(
                                            horizontal: 0.0),
                                        itemBuilder: (context, _) => Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                        ),
                                        onRatingUpdate: (rating) {
                                          print(rating);
                                        },
                                      ),
                                      Text(
                                        "(" +
                                            products[index]['countReview']
                                                .toString() +
                                            ")",
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 11),
                                      ),
                                    ],
                                  ),
                                ),
                                products[index]['discountPercentage'] != 0
                                    ? Row(children: <Widget>[
                                        Container(
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(4.0)),
                                              shape: BoxShape.rectangle,
                                              color: Colors.blueAccent),
                                          padding: EdgeInsets.all(3),
                                          margin: EdgeInsets.only(
                                              top: 2,
                                              bottom: 2,
                                              right: 3,
                                              left: 7),
                                          child: Text(
                                            products[index]
                                                        ['discountPercentage']
                                                    .toString() +
                                                "%",
                                            style: TextStyle(
                                                fontSize: 10.0,
                                                color: Colors.white),
                                          ),
                                        ),
                                        Text(
                                          products[index]['originalPrice'],
                                          style: TextStyle(
                                            fontSize: 10.0,
                                            color: Colors.grey,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ])
                                    : Container(),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 3,
                                    horizontal: 7,
                                  ),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    products[index]['price'],
                                    style: TextStyle(fontSize: 11.0),
                                  ),
                                ),
                              ],
                            )),
                      );
                    },
                    childCount: products.length,
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
